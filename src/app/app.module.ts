import { NgModule,enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AngularFileUploaderModule } from "angular-file-uploader";
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';



import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminLoginDashBoardComponent } from './admin-login-dash-board/admin-login-dash-board.component';

import { HashLocationStrategy, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { ParticipentDashBoardComponent } from './participent-dash-board/participent-dash-board.component';

import { ReasonsComponent } from './reasons/reasons.component';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { NewParticipantComponent } from './new-participant/new-participant.component';
import { MaterialModule } from './material-module';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import {StudentUpdateComponent} from './student-update/student-update.component'
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { HttpClientModule } from '@angular/common/http';
import { ParticipentServiceService } from './participent-service.service';
import { Ng2SearchPipe, Ng2SearchPipeModule } from 'ng2-search-filter';
import { RegisterUserComponent } from './register-user/register-user.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { UserDashBoardComponent } from './UserProfile/user-dash-board/user-dash-board.component';


@NgModule({
  declarations: [
    AppComponent,
    AdminLoginDashBoardComponent,
    routingComponents,
    ParticipentDashBoardComponent,
    ReasonsComponent,
    NewParticipantComponent,
    PrivacyPolicyComponent,
    StudentUpdateComponent,
    RegisterUserComponent,
    ResetPasswordComponent,
    UserDashBoardComponent,
    
    
  ],
  imports: [
    
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    MaterialModule,
    AngularFileUploaderModule,
    NgbDropdownModule,
    Ng2SearchPipeModule,
  BrowserAnimationsModule,
  HttpClientModule,


  // MatTableModule,
  // MatPaginatorModule,
  // MatSortModule
],
  providers: [
    {provide:LocationStrategy ,useClass: HashLocationStrategy},
    AuthGuard,ParticipentServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
