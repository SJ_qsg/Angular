import { AfterViewInit, Component, OnInit } from '@angular/core';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { UntypedFormBuilder, FormControl, UntypedFormGroup, Validators } from '@angular/forms'
import { Search } from '@syncfusion/ej2-angular-grids';
//import { get } from 'http';
//import { ApiService } from './shared/api.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AdminLogin } from './adminLogin-model';
import { ActivatedRoute, Router } from '@angular/router';
import { ParticipentDashBoardComponent } from '../participent-dash-board/participent-dash-board.component';
import { ParticipentServiceService } from '../participent-service.service';

import { ApiService } from '../shared/api.service';
import { keyframes } from '@angular/animations';


@Component({
  selector: 'app-admin-login-dash-board',
  templateUrl: './admin-login-dash-board.component.html',
  styleUrls: ['./admin-login-dash-board.component.css']
})
export class AdminLoginDashBoardComponent implements OnInit, AfterViewInit {
  invalidUserMessage = '';
  formValue !: UntypedFormGroup;
  hide = true;
  authenticated = false;
  public showPassword!: boolean;
  public showPasswordOnPress!: boolean;
  userNAME: any;
  toggleCheckBox: boolean = true;
  forgotPassWord !: boolean;
  constructor(
    private formBuilder: UntypedFormBuilder,
    private _activatedRoute: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private api: ApiService,
    private partiService: ParticipentServiceService) { }

  onAdminSubmit(): void {
    this.router.navigate(['/ParticipentDashBorad'])
  }
  ngAfterViewInit(): void {
    localStorage.removeItem('user');

    //  this.http.post('http://localhost:9070/logout', {},{withCredentials  : true})
    this.api.logout({})
      .subscribe(() => this.authenticated = false);
    localStorage.removeItem('user')
  }
  ngOnInit(): void {
    this.formValue = this.formBuilder.group({
      userName: [''],
      passWord: ['']
    })

  }

  changeEvent(event: any) {
    if (event.target.checked) {
      this.toggleCheckBox = false;
    }
    else {
      this.toggleCheckBox = true;
    }
  }

  errMeasge = document.getElementById('errorMessage');
  submitLogin() {
    // this.http.post('http://localhost:9070/login',this.formValue.getRawValue(),{withCredentials  : true})
    this.api.login(this.formValue.getRawValue())
      .subscribe({
        next: (res:any) => {
          this.userNAME = this.formValue.getRawValue().userName;

          if(res[0].roleId == '1'){
           
          this.router.navigate(['/participentDashBoard'])
          }else if(res[0].roleId == '2'){
            
            this.router.navigate(['/userProfile'])
          }

          
          // this.partiService.communicateUser(this.userNAME)
   
          localStorage.setItem('userId',res[0].userId)
          localStorage.setItem('roleId',res[0].roleId)
        },
        error: (err) => {
          this.forgotPassWord = true;
  
          console.log(err.error)
          alert('Username or Password maybe invalid')
         
          //this.formValue.reset();
        }
      }
      )
    localStorage.setItem('user', this.formValue.getRawValue().userName)

  }




}
