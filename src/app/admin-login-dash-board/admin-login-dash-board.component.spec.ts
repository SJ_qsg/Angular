import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLoginDashBoardComponent } from './admin-login-dash-board.component';

describe('AdminLoginDashBoardComponent', () => {
  let component: AdminLoginDashBoardComponent;
  let fixture: ComponentFixture<AdminLoginDashBoardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminLoginDashBoardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLoginDashBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
