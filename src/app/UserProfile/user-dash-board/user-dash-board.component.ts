import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ParticipantModel } from 'src/app/participant-class-dashboard';
import { ParticipentServiceService } from 'src/app/participent-service.service';
import { ApiService } from 'src/app/shared/api.service';

@Component({
  selector: 'app-user-dash-board',
  templateUrl: './user-dash-board.component.html',
  styleUrls: ['./user-dash-board.component.css']
})
export class UserDashBoardComponent implements OnInit {

  constructor(
    private api: ApiService,
    private partiService: ParticipentServiceService,
    private router: Router,
  ) { }

  UserNAME: any;
  participantModelObj: ParticipantModel = new ParticipantModel();

  ngOnInit(): void {


    this.getStudent()
    this.UserNAME = localStorage.getItem('user')
    
  }


  getStudent() {
    this.api.getUserProfile(localStorage.getItem('userId'))
      .subscribe({
        next: (res:any) => {
         
          // console.log(res)
          this.participantModelObj = res[0]
       
          // console.log(this.participantModelObj)

        },
        error: (err:any) => {
          alert("Error while fetching recoreds")
        }
      })
  }


  onEdit(element: any) {

    this.partiService.communicateParticipant(element);
    console.log(element)
    this.router?.navigate(['/UpdateStudent'])
  };

  logout() {

    // this.http.post('http://localhost:9070/logout', {},{withCredentials:true})
    this.api.logout({})
      .subscribe(() => this.router.navigate(['/therapystudenttracker']));
    localStorage.removeItem('user')
  }





  FeedBack() {
   


    this.api.feedBack(this.participantModelObj.studentId, this.participantModelObj).subscribe({
      next: () => {
        this.getStudent()
      },
      error: (err) => {
        alert(err)
      }
    })
  }

}
