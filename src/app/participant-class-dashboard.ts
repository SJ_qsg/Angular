export class ParticipantModel {
    studentId:string = '';
    name: string = '';
    address: string ='';
    mobile: string = '';
    email: string = '';
    emergencyName: string = '';
    emergencyContact: string = '';
    referredBy: string ='';
    gender :  string ='';
    dateOfBirth : string ='';
    occupation : string ='';
    education : string = '';
    relationship: string = '';
    dateOfJoining : string = '';
    
    height: string = '';
    weight : string= '';
    currentLevelOfActivity: string = '';
    currentRoutine: string = '';
    mostStress: string = '';
    pranayamaPractice: string = '' ;
    anySurgery: string = '';
    anyMedical :  string ='';
    smoked: string = '';
    sleepQuality: string = '';

    reasonForYoga: string = '';
    musculoskeletal: string = '';
    respiratory: string = '';
    cardiovascular:  string ='';
    circulatory:  string = '' ;
    neurological:  string = '';
    gastrointestinal: string = '';
    endocrinological: string = '';
    gynecologicalOrUrological:  string = '';
    pregnancy: string = '';
    otherMedicalConditions: string = '';
    otherInformations: string = '';
    feedBack : string = '';
    createdBy : any = '';
    createdDate : string = '';
    modifiedBy : any = '';
    modifiedDate : string = '';
    activeStatus : string = '';
}

export class FileModel{
    
    fileId     : number = 0;
    studentId : string = '';
	fileName   : string = '';
	fileSize   : string = '';
	createDate : string = '';

}


