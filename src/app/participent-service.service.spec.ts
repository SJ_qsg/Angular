import { TestBed } from '@angular/core/testing';

import { ParticipentServiceService } from './participent-service.service';

describe('ParticipentServiceService', () => {
  let service: ParticipentServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParticipentServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
