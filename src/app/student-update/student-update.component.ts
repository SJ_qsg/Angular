import { Component, OnInit,AfterViewInit } from '@angular/core';
import { UntypedFormBuilder,UntypedFormGroup, Validators } from '@angular/forms';
import { ParticipantModel } from '../participant-class-dashboard';
import { ApiService } from '../shared/api.service';
import { ParticipentServiceService } from '../participent-service.service';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-student-update',
  templateUrl: './student-update.component.html',
  styleUrls: ['./student-update.component.css']
})
export class StudentUpdateComponent implements OnInit,AfterViewInit {

  public formValue!: UntypedFormGroup;
//  fileName= '';
  searchKey : any;
  participantModelObj : ParticipantModel = new ParticipantModel();

 studentId:any;
 UserNAME:any;
 show1 !: boolean
 show2 !: boolean
 show3 !: boolean
 show4 !: boolean

 
 roleID:any;
 

  constructor(private formbuilder : UntypedFormBuilder, 
    private api : ApiService,
    private partiService : ParticipentServiceService,
    private http : HttpClient) { }
  



  ngOnInit(): any {

    this.roleID = localStorage.getItem('roleId')
    
    this.formValue = this.formbuilder.group({


      name:['',[Validators.required,Validators.pattern("^[A-Z][a-zA-Z\\s]+$")]],
      address:['',Validators.required],
      mobile: ['',[Validators.required,Validators.pattern("^[6-9][0-9]{9}$")]],
      email: ['',[Validators.required,Validators.email]],
      emergencyName: ['',[Validators.required,Validators.pattern("^[A-Z][a-zA-Z\\s]+$")]],
      emergencyContact: ['',[Validators.required,Validators.pattern("^[6-9][0-9]{9}$")]],
      referredBy: ['',Validators.required],

      gender :  ['',Validators.required],
      dateOfBirth :['',Validators.required],
      occupation : ['',Validators.required],
      education : ['',Validators.required],
      relationship: ['',Validators.required],
      dateOfJoining :['',Validators.required],
     

      height: ['',Validators.required],
      weight : ['',Validators.required],
      currentLevelOfActivity:['',Validators.required],
      currentRoutine: ['',Validators.required],
      mostStress:['',Validators.required],
      pranayamaPractice: ['',Validators.required],
      anySurgery: ['',Validators.required],
      anyMedical : ['',Validators.required],
      sleepQuality:['',Validators.required],
      smoked:['',Validators.required],
      
      reasonForYoga : ['None',Validators.required],
      musculoskeletal:['',Validators.required],
      respiratory:['',Validators.required],
      cardiovascular:  ['',Validators.required],
      circulatory:  ['',Validators.required],
      neurological:  ['',Validators.required],
      gastrointestinal: ['',Validators.required],
      endocrinological: ['',Validators.required],
      gynecologicalOrUrological: ['',Validators.required],
      pregnancy:['', Validators.required],
      otherMedicalConditions: ['',Validators.required],
      otherInformations: [''],

    })
   

   this.partiService.getParti().subscribe(res => {
   if(res){
    this.fillTheForm(res);
    this.studentId=res.studentId

    }
     })




    // this.partiService.getUser().subscribe(user => {
    //   this.UserNAME = user
    // })

     this.UserNAME= localStorage.getItem('user')

   
    this.showStep1();


  }


  


  showStep1(){
    this.show1=true;
    this.show2=false;
    this.show3=false;
    this.show4=false;
   let elem1 :  HTMLElement =<any> document.getElementById('step1')
   elem1.setAttribute("style", "border: 2px solid #4285F4; color: #4285F4 !important;background-color: white !important;" )

   let elem2 :  HTMLElement =<any> document.getElementById('step2')
   elem2.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

   let elem3 :  HTMLElement =<any> document.getElementById('step3')
   elem3.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

   let elem4:  HTMLElement =<any> document.getElementById('step4')
   elem4.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

  }
showStep2(){
  this.show1=false;
  this.show2=true;
  this.show3=false;
  this.show4=false;
  let elem2 :  HTMLElement =<any> document.getElementById('step2')
   elem2.setAttribute("style", "border: 2px solid #4285F4; color: #4285F4 !important;background-color: white !important;" )

   let elem1 :  HTMLElement =<any> document.getElementById('step1')
   elem1.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

   let elem3 :  HTMLElement =<any> document.getElementById('step3')
   elem3.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

   let elem4:  HTMLElement =<any> document.getElementById('step4')
   elem4.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

//    setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )
// setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )
// setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

}
showStep3(){
  this.show1=false;
  this.show2=false;
  this.show3=true;
  this.show4=false;
  let elem3 :  HTMLElement =<any> document.getElementById('step3')
   elem3.setAttribute("style", "border: 2px solid #4285F4; color: #4285F4 !important;background-color: white !important;" )

   let elem1 :  HTMLElement =<any> document.getElementById('step1')
   elem1.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

   let elem2 :  HTMLElement =<any> document.getElementById('step2')
   elem2.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

   let elem4:  HTMLElement =<any> document.getElementById('step4')
   elem4.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

}
showStep4(){
  this.show1=false;
  this.show2=false;
  this.show3=false;
  this.show4=true;
  let elem4 :  HTMLElement =<any> document.getElementById('step4')
   elem4.setAttribute("style", "border: 2px solid #4285F4; color: #4285F4 !important;background-color: white !important;" )

   let elem1 :  HTMLElement =<any> document.getElementById('step1')
   elem1.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

   let elem3 :  HTMLElement =<any> document.getElementById('step3')
   elem3.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

   let elem2:  HTMLElement =<any> document.getElementById('step2')
   elem2.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

}

  fillTheForm(res:any){
    
    this.formValue.patchValue({
      name:res.name,
      address:res.address,
      mobile: res.mobile,
      email:res.email,
      emergencyName: res.emergencyName,
      emergencyContact: res.emergencyContact,
      referredBy:res.referredBy,

      gender : res.gender,
      dateOfBirth :res.dateOfBirth,
      occupation : res.occupation,
      education : res.education,
      relationship: res.relationship,
      dateOfJoining :res.dateOfJoining,
  

      height: res.height,
      weight : res.weight,
      currentLevelOfActivity:res.currentLevelOfActivity,
      currentRoutine: res.currentRoutine,
      mostStress: res.mostStress,
      pranayamaPractice: res.pranayamaPractice,
      anySurgery: res.anySurgery,
      anyMedical :res.anyMedical,
      sleepQuality:res.sleepQuality,
      smoked: res.smoked,
      

      reasonForYoga : res.reasonForYoga,
      musculoskeletal: res.musculoskeletal,
      respiratory: res.respiratory,
      cardiovascular:  res.cardiovascular,
      circulatory:  res.circulatory,
      neurological:  res.neurological,
      gastrointestinal: res.gastrointestinal,
      endocrinological: res.endocrinological,
      gynecologicalOrUrological: res.gynecologicalOrUrological,
      pregnancy:res.pregnancy,
      otherMedicalConditions: res.otherMedicalConditions,
      otherInformations: res.otherInformations,
    })
  

  }


  updateStudent(){
    

  this.participantModelObj.name = this.formValue.value.name;
    this.participantModelObj.address = this.formValue.value.address;
    this.participantModelObj.mobile = this.formValue.value.mobile;
    this.participantModelObj.email = this.formValue.value.email;
    this.participantModelObj.emergencyName = this.formValue.value.emergencyName;
    this.participantModelObj.emergencyContact = this.formValue.value.emergencyContact;
    this.participantModelObj.referredBy = this.formValue.value.referredBy;
    this.participantModelObj.gender = this.formValue.value.gender;
    this.participantModelObj.dateOfBirth = this.formValue.value.dateOfBirth;
    this.participantModelObj.occupation = this.formValue.value.occupation;
    this.participantModelObj.education = this.formValue.value.education;
    this.participantModelObj.relationship = this.formValue.value.relationship;
    this.participantModelObj.dateOfJoining = this.formValue.value.dateOfJoining;
    this.participantModelObj.height = this.formValue.value.height;
    this.participantModelObj.weight = this.formValue.value.weight;
    this.participantModelObj.currentLevelOfActivity = this.formValue.value.currentLevelOfActivity;
    this.participantModelObj.currentRoutine = this.formValue.value.currentRoutine;
    this.participantModelObj.mostStress = this.formValue.value.mostStress;
    this.participantModelObj.pranayamaPractice = this.formValue.value.pranayamaPractice
    this.participantModelObj.anySurgery = this.formValue.value.anySurgery;
    this.participantModelObj.anyMedical = this.formValue.value.anyMedical;
    this.participantModelObj.sleepQuality = this.formValue.value.sleepQuality;
    this.participantModelObj.smoked = this.formValue.value.smoked;
  
    this.participantModelObj.reasonForYoga = this.formValue.value.reasonForYoga;
    this.participantModelObj.musculoskeletal = this.formValue.value.musculoskeletal;
    this.participantModelObj.respiratory = this.formValue.value.respiratory;
    this.participantModelObj.cardiovascular = this.formValue.value.cardiovascular;
    this.participantModelObj.circulatory = this.formValue.value.circulatory;
    this.participantModelObj.neurological = this.formValue.value.neurological;
    this.participantModelObj.gastrointestinal = this.formValue.value.gastrointestinal;
    this.participantModelObj.endocrinological = this.formValue.value.endocrinological;
    this.participantModelObj.gynecologicalOrUrological = this.formValue.value.gynecologicalOrUrological;
    this.participantModelObj.pregnancy = this.formValue.value.pregnancy;
    this.participantModelObj.otherMedicalConditions = this.formValue.value.otherMedicalConditions;
    this.participantModelObj.otherInformations = this.formValue.value.otherInformations;
    this.participantModelObj.modifiedBy = localStorage.getItem('userId')?.toString();
    

    this.api.updateStudent(this.studentId,this.participantModelObj)
    .subscribe({
          next:(res)=>{
            // alert("Student updated successfully")
            const msg ="update was successfull"
            this.partiService.communicateParticipant(msg)
            this.formValue.reset();
         
          },
          error:()=>{
            alert("Error while updating student")
          }
        })






  }




  

  get file(){
    return this.formValue.get('file')
  }


  get name(){
     return this.formValue.get('name');
  }

  get address(){
    return this.formValue.get('address');
  }

  get mobile() {
    return this.formValue.get('mobile');
  }

  get email() {
    return this.formValue.get('email');
  }

  get emergencyName() {
    return this.formValue.get('emergencyName');
  }

  get emergencyContact() {
    return this.formValue.get('emergencyContact');
  }

  get referredBy() {
    return this.formValue.get('referredBy');
  }

  get gender() {
    return this.formValue.get('gender');
  }

  get dateOfBirth() {
    return this.formValue.get('dateOfBirth');
  }

  get occupation() {
    return this.formValue.get('occupation');
  }

  get education() {
    return this.formValue.get('education');
  }

  get relationship() {
    return this.formValue.get('relationship');
  }

  get dateOfJoining() {
    return this.formValue.get('dateOfJoining');
  }

  get height() {
    return this.formValue.get('height');
  }

  get weight() {
    return this.formValue.get('weight');
  }

  get levelOfActivity() {
    return this.formValue.get('currentLevelOfActivity');
  }

  get currentRoutine() {
    return this.formValue.get('currentRoutine');
  }

  get mostStress() {
    return this.formValue.get('mostStress');
  }

  get pranayama() {
    return this.formValue.get('pranayamaPractice');
  }

  get anySurgery() {
    return this.formValue.get('anySurgery');
  }

  get anyMedical() {
    return this.formValue.get('anyMedical');
  }

get smoked(){
  return this.formValue.get('smoked')
}

get sleepQuality(){
  return this.formValue.get('sleepQuality')
}
   
  get reasonForYoga() {
    return this.formValue.get('reasonForYoga');
  }

  get musculoskeletal() {
    return this.formValue.get('musculoskeletal');
  }

  get respiratory() {
    return this.formValue.get('respiratory');
  }

  get cardiovascular() {
    return this.formValue.get('cardiovascular');
  }

  get circulatory() {
    return this.formValue.get('circulatory');
  }

  get neurological() {
    return this.formValue.get('neurological');
  }

  get gastrointestinal() {
    return this.formValue.get('gastrointestinal');
  }

  get endocrinological() {
    return this.formValue.get('endocrinological');
  }

  get gynecological() {
    return this.formValue.get('gynecologicalOrUrological');
  }

  get pregnancy() {
    return this.formValue.get('pregnancy')
  }

  get otherMedicalConditons() {
    return this.formValue.get('otherMedicalConditions');
  }

  get otherInformation() {
    return this.formValue.get('otherInformations');
  }

  
 
   ngAfterViewInit(): void {
    
    
    
  }

  

}


