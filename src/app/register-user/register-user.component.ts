import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../shared/api.service';
import { UserModel } from './user-Model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  UserModelObj : UserModel = new UserModel();
formValue !:FormGroup;
toggleCheckBox: boolean=true;

showEmail: boolean = true;
  showCode!: boolean;
  showRegisterForm!: boolean;

public showPassword!: boolean;
public showConfirmPassword!: boolean;

EmailExists!:any
  constructor(
    private api: ApiService,
    private formBuilder : FormBuilder,
    private router : Router
  ) { }

  ngOnInit(): void {
this.formValue = this.formBuilder.group({
  
	userName :new FormControl('',[Validators.required,Validators.pattern("^[a-zA-Z0-9\\s]+$")]),
	passWord :new FormControl('',[Validators.required]),
  ConfirmPassWord :new FormControl('',[Validators.required]),
	firstName :new FormControl('',[Validators.required,Validators.pattern("^[a-zA-Z\\s]+$")]),
	lastName :new FormControl('',[Validators.required,Validators.pattern("^[a-zA-Z\\s]+$")]),
	contactNumber :new FormControl('',[Validators.required,Validators.pattern("^[6-9][0-9]{9}$")]),
	email :new FormControl('',[Validators.required, Validators.email]),
  code: ['', Validators.required],
},{validators:[this.Mustmatch('passWord','ConfirmPassWord'),this.CheckUserName('userName')]})
  }

  // ,this.CheckEmail('email'),this.CheckUserName('userName')

  // ,[{
  //   validators:this.Mustmatch('passWord','ConfirmPassWord')},{
  //   validators:this.CheckEmail('email')},{
  //   validators:this.CheckUserName('userName')
  // }]
  get userName() {
    return this.formValue.get('userName')
  }

  get passWord() {
    return this.formValue.get('passWord')
  }

  get confirmPassWord() {
    return this.formValue.get('ConfirmPassWord')
  }

  get firstName() {
    return this.formValue.get('firstName')
  }

  get lastName() {
    return this.formValue.get('lastName')
  }

  get contactNumber() {
    return this.formValue.get('contactNumber')
  }

  get email() {
    return this.formValue.get('email')
  }

  get code() {
    return this.formValue.get('code')
  }

Mustmatch(passWord:any, confirmPassWord:any){
  return (FormGroup: FormGroup)=> {
    const passWordControl = FormGroup.controls[passWord];
    const ConfirmpassWordControl = FormGroup.controls[confirmPassWord];
    
    if(ConfirmpassWordControl.errors && !ConfirmpassWordControl.errors['Mustmatch']) {
      return;
    }

    if( passWordControl.value!==ConfirmpassWordControl.value) {
      ConfirmpassWordControl.setErrors({Mustmatch:true})
    } else {
      ConfirmpassWordControl.setErrors(null);
    }
  }
}

CheckUserName(userName:any){
return  (FormGroup: FormGroup) => {
  const UserName = FormGroup.controls[userName]
 
 var data = UserName.value
  this.api.checkUserName(data).subscribe({
    next:(res)=>{
      if(res!=null) {
        UserName.setErrors({UserNameExists:true})
     
      }else{
        UserName.setErrors(null)
      }
     
     
    },
    error:()=>{
      UserName.setErrors(null)
     
    }
  })
}
}

// CheckEmail(email:any){
//   return  (FormGroup: FormGroup) => {
//     const Email = FormGroup.controls[email]

//     var data = Email.value
    
//     this.api.checkEmail(data).subscribe({
//       next:(res)=>{
//         if(res!=null) {
//           Email.setErrors({EmailExists:true})
//           }else{
          
//             Email.setErrors(null)
//           }
         
//       },
//       error:()=>{
//         Email.setErrors(null)
        
//       }
//     })
//   }
  

// }





changeEvent(event:any){
  if (event.target.checked) {
    this.toggleCheckBox= false;
}
else {
    this.toggleCheckBox= true;
}
}


SubmitEmail() {

  var Email = this.formValue.getRawValue().email

   
    
    this.api.registerEmailCheck(Email).subscribe({
      next:()=>{
        this.EmailExists = "This Email-Id already registered"
        setTimeout(() =>{
          this.EmailExists = " "
        },3000)
        
      },
      error:()=>{
        
        this.showEmail = false
        this.showCode = true
        this.showRegisterForm = false
        
      }
    })
  
  
  


 
  
}

// sendCode() {
//   var code = this.formValue.getRawValue().code
 
//   this.api.resetPasswordEmail(code).subscribe({
//     next:()=>{
//       alert("Verification code sent to your Email")
    
//     },error:()=>{
//       alert("Something went wrong")
//     }
//   })

// }

SubmitCode() {
var code = this.formValue.getRawValue().code

this.api.resetPassWordCode(code).subscribe({
next:()=>{
  this.showEmail = false
  this.showCode = false
  this.showRegisterForm = true
},error:()=>{
  alert("Code did not match")
}
})

}
// user.UserName, user.Password, user.FirstName, user.LastName, user.ContactNumber, user.Email
submitRegister(){
  this.UserModelObj.userName = this.formValue.getRawValue().userName;
  this.UserModelObj.passWord = this.formValue.getRawValue().passWord;
  this.UserModelObj.firstName = this.formValue.getRawValue().firstName;
  this.UserModelObj.lastName = this.formValue.getRawValue().lastName;
  this.UserModelObj.contactNumber = this.formValue.getRawValue().contactNumber;
  this.UserModelObj.email = this.formValue.getRawValue().email;
  this.UserModelObj.roleId = '2';
  
  this.api.register(this.UserModelObj).subscribe({
    next:()=>{
      alert('Registered Successfully')
      this.router.navigate(['/therapystudenttracker'])
  },
  error:()=>{
    alert('Registration failed')
  }
})
}


}
