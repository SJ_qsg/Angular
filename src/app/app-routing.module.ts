import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewParticipantComponent } from './new-participant/new-participant.component';
import { AdminLoginDashBoardComponent } from './admin-login-dash-board/admin-login-dash-board.component';
import { ParticipentDashBoardComponent } from './participent-dash-board/participent-dash-board.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import {StudentUpdateComponent} from './student-update/student-update.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import { ReasonsComponent } from './reasons/reasons.component';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { UserDashBoardComponent } from './UserProfile/user-dash-board/user-dash-board.component';

const routes: Routes = [
    {path:'', redirectTo:'therapystudenttracker', pathMatch:'full'},
   {path:'therapystudenttracker',component: AdminLoginDashBoardComponent },
   {path:'participentDashBoard',component: ParticipentDashBoardComponent, canActivate:[AuthGuard]},
  
  {path:'newparticipent', component: NewParticipantComponent},
 
  {path:'reasons', component:ReasonsComponent},
  {path:'privacy', component: PrivacyPolicyComponent},
  {path:'UpdateStudent', component: StudentUpdateComponent},
  {path:'register', component :RegisterUserComponent},
  {path:'resetPassWord', component:ResetPasswordComponent},

  {path:'userProfile', component: UserDashBoardComponent,  canActivate:[AuthGuard]}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers:[AuthGuard,AuthService]
})
export class AppRoutingModule { }

export const routingComponents = [NewParticipantComponent]
