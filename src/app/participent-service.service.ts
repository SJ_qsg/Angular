import { Injectable } from '@angular/core';
import {Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ParticipentServiceService {
  sendParticipent = new BehaviorSubject<any>([]);
  senduser = new BehaviorSubject<any>([])
  constructor() {}

  communicateParticipant(parti:any) {
    this.sendParticipent.next(parti)
  }

  getParti(): Observable<any> {
    return this.sendParticipent.asObservable();
  }

// --------------------------------------------------------
  communicateUser(user:string) {
    this.senduser.next(user)
  }

  getUser(): Observable<string> {
    return this.senduser.asObservable();
  }

// --------------------------------------------------------------

communicateBufferFlag(flag:boolean){
  this.senduser.next(flag)
}

getBufferFlag():Observable<boolean> {
  return this.senduser.asObservable();
}

}


