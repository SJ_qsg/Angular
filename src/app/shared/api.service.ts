
import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { EmailValidator } from '@angular/forms';
import { environment } from 'src/environments/environment';

import { AdminLogin } from '../admin-login-dash-board/adminLogin-model';



@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // part:any="https://yogamandira.com:9080"

  constructor(private http: HttpClient) { }

  postStudent(data:any){
    
    return this.http.post<any>(environment.api+"/students",data,{withCredentials  : true})
  }

  public getStudent(){
  //   const headers  = new HttpHeaders()
  //   .set('content-type', 'application/json')
  //  .set('Access-Control-Allow-Origin', 'http://yogamandira:4200');
    return this.http.get<any>(environment.api+"/students",{withCredentials  : true});
  }


  public getUserProfile(UserId:any){
    //   const headers  = new HttpHeaders()
    //   .set('content-type', 'application/json')
    //  .set('Access-Control-Allow-Origin', 'http://yogamandira:4200');
      return this.http.get<any>(environment.api+"/students/"+UserId,{withCredentials  : true});
    }

  deleteStudent(studentId:any){
    return this.http.delete<any>(environment.api+"/students/"+studentId, {withCredentials  : true});
  }

  updateStudent(studentId:any, data:any){
    return this.http.put<any>(environment.api+"/students/"+studentId,data,{withCredentials  : true});
  }

  login(data:any){
    return this.http.post<any>(environment.api+"/login",data,{withCredentials:true});
  }

  logout(data:any) {
    return this.http.post<any>(environment.api+"/logout",data,{withCredentials:true});
  }



  register(data:any) {
    return this.http.post<any>(environment.api+"/register",data)
  }

  registerEmailCheck(data:any){
    return this.http.get<any>(environment.api+"/ValidateRegisterEmail/"+data)
  }

  checkUserName(data:any) {
    return this.http.get<any>(environment.api+"/CheckUserName/"+data)
  }

  checkEmail(data:any) {
    return this.http.get<any>(environment.api+"/CheckEmail/"+data)
  }

  resetPasswordEmail(data:any) {
    return this.http.get<any>(environment.api+"/ValidateEmail/"+data)
  }

resetPassWordCode(data:any) {
  return this.http.get<any>(environment.api+"/VerifyCode/"+data)
}

resetPassWord(data:any){
  return this.http.put<any>(environment.api+"/PassWordReset",data)
}



fileMetaData(data:any){
  return this.http.post<any>(environment.api+"/FileData",data,{withCredentials:true})
}

feedBack(studentId:any, data:any){
  return this.http.put<any>(environment.api+"/feedBack/"+studentId,data,{withCredentials:true})
}


}

