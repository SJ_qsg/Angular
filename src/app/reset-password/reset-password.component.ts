import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PassWordReset } from './reset-password-model';
import { ApiService } from '../shared/api.service';
import { UserModel } from '../register-user/user-Model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  showEmail: boolean = true
  showCode!: boolean;
  showPassWord!: boolean;
  formValue !: FormGroup
  USerModelObj:UserModel=new UserModel()
  EmailRegistered !: any
  constructor(
    private formBuilder: FormBuilder,
    private api : ApiService,
    private router : Router
  ) { }

  ngOnInit(): void {
    this.formValue = this.formBuilder.group({
      email: ['', Validators.required],
      code: ['', Validators.required],
      passWord: ['', Validators.required],
      confirmPassWord: ['', Validators.required]
    }, { validators: [this.Mustmatch('passWord', 'confirmPassWord')] })

  }


  get email() {
    return this.formValue.get('email')
  }

  get code() {
    return this.formValue.get('code')
  }

  get passWord() {
    return this.formValue.get('passWord')
  }

  get confirmPassWord() {
    return this.formValue.get('confirmPassWord')
  }

  SubmitEmail() {
    var Email = this.formValue.getRawValue().email
 
 
    this.api.resetPasswordEmail(Email).subscribe({
      next:()=>{
        
        this.EmailRegistered="Email is not registered"
        setTimeout(() =>{
          this.EmailRegistered = " "
        },3000)
     
      },error:()=>{
      
        this.showEmail = false
        this.showCode = true
        this.showPassWord = false
      }
    })
    
  }

  SubmitCode() {
var code = this.formValue.getRawValue().code

this.api.resetPassWordCode(code).subscribe({
  next:()=>{
    this.showEmail = false
    this.showCode = false
    this.showPassWord = true
  },error:()=>{
    alert("Code did not match")
  }
})

   
  }



  submitResetPassword(){
    this.USerModelObj.passWord = this.formValue.getRawValue().passWord
    this.USerModelObj.email = this.formValue.getRawValue().email
    this.api.resetPassWord(this.USerModelObj).subscribe({
      next:()=>{
        alert("Password was reset")
        this.router.navigate(['/therapystudenttracker'])
      },error:()=>{
        alert("Password reset failed")
        this.router.navigate(['/therapystudenttracker'])
      }
    })
  }

  Mustmatch(passWord: any, confirmPassWord: any) {
    return (FormGroup: FormGroup) => {
      const passWordControl = FormGroup.controls[passWord];
      const ConfirmpassWordControl = FormGroup.controls[confirmPassWord];

      if (ConfirmpassWordControl.errors && !ConfirmpassWordControl.errors['Mustmatch']) {
        return;
      }

      if (passWordControl.value !== ConfirmpassWordControl.value) {
        ConfirmpassWordControl.setErrors({ Mustmatch: true })
      } else {
        ConfirmpassWordControl.setErrors(null);
      }
    }
  }


}
