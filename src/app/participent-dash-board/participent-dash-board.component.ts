import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { UntypedFormBuilder, FormControl, UntypedFormGroup, Validators } from '@angular/forms'
import { MatSort } from '@angular/material/sort';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { ApiService } from '../shared/api.service';
import { Emitters } from '../Emitters/Emitters';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ParticipentServiceService } from '../participent-service.service';
import { Router, Event, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import * as FileSaver from 'file-saver';
import * as saveAs from 'file-saver';
import { HostListener } from '@angular/core';
import { ParticipantModel } from '../participant-class-dashboard';



@Component({
  selector: 'app-participent-dash-board',
  templateUrl: './participent-dash-board.component.html',
  styleUrls: ['./participent-dash-board.component.css']
})

export class ParticipentDashBoardComponent implements OnInit {
  participantModelObj: ParticipantModel = new ParticipantModel();
  createdMessage !: ''
  formValue!: UntypedFormGroup;
  searchKey: any
  authenticated = false;
  UserNAME: any;
  public getScreenWidth: any;
  public getScreenHeight: any;
  showEmergencyContact !: boolean;
  showLoadingIndicator !: boolean;
  buffer!: boolean
  // displayedColumns: string[] = ['name', 'address', 'mobile', 'email', 'emergencyContactName', 'emergencyContactNumber', 'referredBy','action'];
  displayedColumns: string[] = ['name', 'mobile', 'email', 'emergencyContact', 'action'];
  dataSource !: MatTableDataSource<any>;

  roleID: any

  @ViewChild(MatPaginator) paginator !: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(
    private formbuilder: UntypedFormBuilder,
    private api: ApiService,
    private http: HttpClient,
    private partiService: ParticipentServiceService,
    private router: Router,

  ) {
    this.router.events.subscribe((routerEvent: Event) => {
      if (routerEvent instanceof NavigationStart) {
        this.showLoadingIndicator = true;
      }

      if (routerEvent instanceof NavigationEnd || routerEvent instanceof NavigationCancel || routerEvent instanceof NavigationError) {
        this.showLoadingIndicator = false;
      }
    })


  }


  // ngAfterViewInit(): void {




  //   this.roleID = localStorage.getItem('roleId')

  //   if (this.roleID == '2') {
  //     this.getStudent()
  //   } else if (this.roleID == '1') {
  //     this.getAllStudents();
  //   }



  // }





  ngOnInit(): void {

    this.formValue = this.formbuilder.group({
      feedBack: ['']
    })

    this.getScreenWidth = window.innerWidth;
    this.getScreenHeight = window.innerHeight;

    // this.http.get('http://localhost:9000/user',{withCredentials:true}).subscribe(
    //   (res:any)=>{
    //     Emitters.authEmitter.emit(true);
    //   },  
    //   _err => {
    //     Emitters.authEmitter.emit(false);
    //   }
    // );



    if (this.getScreenWidth <= 600) {
      this.showEmergencyContact = false;
    }



 this.roleID = localStorage.getItem('roleId')

    this.partiService.getParti().subscribe(res => {

      if (res == "New student is inserted" || res == "update was successfull") {
        this.sendMessage(res)
        this.getStudentsBasedOnRoles();
      } else {
        this.sendMessage("")
        this.getStudentsBasedOnRoles();
      }
    })
    this.UserNAME = localStorage.getItem('user')
   
this.getStudentsBasedOnRoles();

  }

  @HostListener('window:resize', ['$event'])
  onWindowResize() {
    this.getScreenWidth = window.innerWidth;
    this.getScreenHeight = window.innerHeight;
  }




  sendMessage(res: any) {
    if (res != []) {
      this.createdMessage = res
    }
    setTimeout(() => {
      this.createdMessage = ""
    }, 3000);
  }


  getStudentsBasedOnRoles(){
    if (this.roleID == '2') {
      console.log("User")
      this.getStudent()
    } else if (this.roleID == '1') {

      console.log("Admin")
      this.getAllStudents();
    }

  }


  getStudent() {
    this.api.getUserProfile(localStorage.getItem('userId'))
      .subscribe({
        next: (res) => {

          this.dataSource = new MatTableDataSource(res);



          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort
        },
        error: (_err) => {
          alert("Error while fetching recoreds")
        }
      })
  }

  getAllStudents() {
    this.api.getStudent()
      .subscribe({
        next: (res) => {

          this.dataSource = new MatTableDataSource(res);



          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort
        },
        error: (_err) => {
          alert("Error while fetching recoreds")
        }
      })
  }

  deleteEmployee(studentId: any, name: any) {
    if (confirm("Are you sure to delete " + name)) {
      this.api.deleteStudent(studentId)
        .subscribe(res => {
          //  alert(name +" was deleted");
          this.sendMessage(name + " was deleted");
          this.getStudentsBasedOnRoles();
        })
    }
  }

  stdId !: any;


  fillFeedBack(element: any) {
    this.formValue.patchValue({
      feedBack: element.feedBack
    })
    this.stdId = element.studentId
  }


  FeedBack(studentId: any) {
    this.participantModelObj.feedBack = this.formValue.value.feedBack;


    this.api.feedBack(this.stdId, this.participantModelObj).subscribe({
      next: () => {
        this.router.navigate(['/participentDashBoard'])
        this.getStudentsBasedOnRoles();
      },
      error: (err) => {
        alert(err)
      }
    })
  }

  dataLength() {
    return this.dataSource.data.length
  }

  onEdit(element: any) {

    this.partiService.communicateParticipant(element);
    console.log(element)
    this.router?.navigate(['/UpdateStudent'])
  };


  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLowerCase();
  }

  clearSearch() {
    this.searchKey = "";
    this.applyFilter();
  }

  logout() {

    // this.http.post('http://localhost:9070/logout', {},{withCredentials:true})
    this.api.logout({})
      .subscribe(() => this);
    localStorage.removeItem('user')
  }

}


