import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipentDashBoardComponent } from './participent-dash-board.component';

describe('ParticipentDashBoardComponent', () => {
  let component: ParticipentDashBoardComponent;
  let fixture: ComponentFixture<ParticipentDashBoardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParticipentDashBoardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipentDashBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
