import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { FileModel, ParticipantModel } from '../participant-class-dashboard';
import { ApiService } from '../shared/api.service';
import { ParticipentServiceService } from '../participent-service.service';
import {ActivatedRoute, Router } from '@angular/router';


import { HttpClient } from '@angular/common/http';
import { async } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-new-participant',
  templateUrl: './new-participant.component.html',
  styleUrls: ['./new-participant.component.css']
})
export class NewParticipantComponent implements OnInit {
  public formValue!: UntypedFormGroup;
  myscript !: any;
  searchKey: any;
  participantModelObj: ParticipantModel = new ParticipantModel();
  FileModelObj: FileModel = new FileModel();
  UserNAME: any;
  stepperElemnt: any;
  show1 !: boolean
  show2 !: boolean
  show3 !: boolean
  show4 !: boolean
  fileSelected!: FileList
  folderId !: Number
  fileUploadCount : number = 0
  bufferFlag !:boolean;

  roleID:any;

  
  constructor(private formbuilder: UntypedFormBuilder,
    private _activatedRoute: ActivatedRoute ,
    private router : Router,
    private api: ApiService,
    private partiSerive: ParticipentServiceService,
    private http: HttpClient,
    ) {
    this.myscript = document.addEventListener
    this.myscript.src = "src/assets/newparti.js"
  }




  ngOnInit(): void {


    this.roleID = localStorage.getItem('roleId')


    this.formValue = this.formbuilder.group({


      name: ['', [Validators.required, Validators.pattern("^[A-Z][a-zA-Z\\s]+$")]],
      address: ['', Validators.required],
      mobile: ['', [Validators.required, Validators.pattern("^[6-9][0-9]{9}$")]],
      email: ['', [Validators.required, Validators.email]],
      emergencyName: ['', [Validators.required, Validators.pattern("^[A-Z][a-zA-Z\\s]+$")]],
      emergencyContact: ['', [Validators.required, Validators.pattern("^[6-9][0-9]{9}$")]],
      referredBy: ['', Validators.required],

      gender: ['', Validators.required],
      dateOfBirth: ['', Validators.required],
      occupation: ['', Validators.required],
      education: ['', Validators.required],
      relationship: ['', Validators.required],
      dateOfJoining: ['', Validators.required],


      height: ['', Validators.required],
      weight: ['', Validators.required],
      currentLevelOfActivity: ['', Validators.required],
      currentRoutine: ['', Validators.required],
      mostStress: ['', Validators.required],
      pranayamaPractice: ['', Validators.required],
      anySurgery: ['', Validators.required],
      anyMedical: ['', Validators.required],
      smoked: ['', Validators.required],
      sleepQuality:['',Validators.required],

      reasonForYoga: [''],
      musculoskeletal: ['', Validators.required],
      respiratory: ['', Validators.required],
      cardiovascular: ['', Validators.required],
      circulatory: ['', Validators.required],
      neurological: ['', Validators.required],
      gastrointestinal: ['', Validators.required],
      endocrinological: ['', Validators.required],
      gynecologicalOrUrological: ['', Validators.required],
      pregnancy: ['', Validators.required],
      otherMedicalConditions: ['', Validators.required],
      otherInformations: [''],


    })
    // this.partiSerive.getUser().subscribe(res => {
    //   this.UserNAME = res
    // })
    this.UserNAME = localStorage.getItem('user')
    this.showStep1();



  }

  showStep1() {
    this.show1 = true;
    this.show2 = false;
    this.show3 = false;
    this.show4 = false;
    let elem1: HTMLElement = <any>document.getElementById('step1')
    elem1.setAttribute("style", "border: 2px solid #4285F4; color: #4285F4 !important;background-color: white !important;")

    let elem2: HTMLElement = <any>document.getElementById('step2')
    elem2.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;")

    let elem3: HTMLElement = <any>document.getElementById('step3')
    elem3.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;")

    let elem4: HTMLElement = <any>document.getElementById('step4')
    elem4.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;")

  }
  showStep2() {
    this.show1 = false;
    this.show2 = true;
    this.show3 = false;
    this.show4 = false;
    let elem2: HTMLElement = <any>document.getElementById('step2')
    elem2.setAttribute("style", "border: 2px solid #4285F4; color: #4285F4 !important;background-color: white !important;")

    let elem1: HTMLElement = <any>document.getElementById('step1')
    elem1.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;")

    let elem3: HTMLElement = <any>document.getElementById('step3')
    elem3.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;")

    let elem4: HTMLElement = <any>document.getElementById('step4')
    elem4.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;")

    //    setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )
    // setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )
    // setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;" )

  }
  showStep3() {
    this.show1 = false;
    this.show2 = false;
    this.show3 = true;
    this.show4 = false;
    let elem3: HTMLElement = <any>document.getElementById('step3')
    elem3.setAttribute("style", "border: 2px solid #4285F4; color: #4285F4 !important;background-color: white !important;")

    let elem1: HTMLElement = <any>document.getElementById('step1')
    elem1.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;")

    let elem2: HTMLElement = <any>document.getElementById('step2')
    elem2.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;")

    let elem4: HTMLElement = <any>document.getElementById('step4')
    elem4.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;")

  }
  showStep4() {
    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.show4 = true;
    let elem4: HTMLElement = <any>document.getElementById('step4')
    elem4.setAttribute("style", "border: 2px solid #4285F4; color: #4285F4 !important;background-color: white !important;")

    let elem1: HTMLElement = <any>document.getElementById('step1')
    elem1.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;")

    let elem3: HTMLElement = <any>document.getElementById('step3')
    elem3.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;")

    let elem2: HTMLElement = <any>document.getElementById('step2')
    elem2.setAttribute("style", "border: 2px solid #59698D; color: #ffc107 !important;background-color: #4285F4 !important;")

  }











  get file() {
    return this.formValue.get('file')
  }


  get name() {
    return this.formValue.get('name');
  }

  get address() {
    return this.formValue.get('address');
  }

  get mobile() {
    return this.formValue.get('mobile');
  }

  get email() {
    return this.formValue.get('email');
  }

  get emergencyName() {
    return this.formValue.get('emergencyName');
  }

  get emergencyContact() {
    return this.formValue.get('emergencyContact');
  }

  get referredBy() {
    return this.formValue.get('referredBy');
  }

  get gender() {
    return this.formValue.get('gender');
  }

  get dateOfBirth() {
    return this.formValue.get('dateOfBirth');
  }

  get occupation() {
    return this.formValue.get('occupation');
  }

  get education() {
    return this.formValue.get('education');
  }

  get relationship() {
    return this.formValue.get('relationship');
  }

  get dateOfJoining() {
    return this.formValue.get('dateOfJoining');
  }

  get height() {
    return this.formValue.get('height');
  }

  get weight() {
    return this.formValue.get('weight');
  }

  get levelOfActivity() {
    return this.formValue.get('currentLevelOfActivity');
  }

  get currentRoutine() {
    return this.formValue.get('currentRoutine');
  }

  get mostStress() {
    return this.formValue.get('mostStress');
  }

  get pranayama() {
    return this.formValue.get('pranayamaPractice');
  }

  get anySurgery() {
    return this.formValue.get('anySurgery');
  }

  get anyMedical() {
    return this.formValue.get('anyMedical');
  }

  get smoked() {
    return this.formValue.get('smoked')
  }

  get sleepQuality() {
    return this.formValue.get('sleepQuality')
  }

  get reasonForYoga() {
    return this.formValue.get('reasonForYoga');
  }

  get musculoskeletal() {
    return this.formValue.get('musculoskeletal');
  }

  get respiratory() {
    return this.formValue.get('respiratory');
  }

  get cardiovascular() {
    return this.formValue.get('cardiovascular');
  }

  get circulatory() {
    return this.formValue.get('circulatory');
  }

  get neurological() {
    return this.formValue.get('neurological');
  }

  get gastrointestinal() {
    return this.formValue.get('gastrointestinal');
  }

  get endocrinological() {
    return this.formValue.get('endocrinological');
  }

  get gynecological() {
    return this.formValue.get('gynecologicalOrUrological');
  }

  get pregnancy() {
    return this.formValue.get('pregnancy')
  }

  get otherMedicalConditons() {
    return this.formValue.get('otherMedicalConditions');
  }

  get otherInformation() {
    return this.formValue.get('otherInformations');
  }



  getFile(event: any) {
    this.fileSelected = event.target.files;
  }

  onVerfiy(){
   
		var count = 0;
		for (let i = 0; i < this.fileSelected.length; i++) {
     
		if(this.fileSelected[i].size>10240000){
			alert(this.fileSelected[i].name + " exceeds size limit , please remove this")
			count++;
		}	
		
		
	  }
	if(	count==0){
    
		alert("you can upload ")
		
	}
	
		
	 
	}


  addStudent() {



    this.participantModelObj.name = this.formValue.value.name;
    this.participantModelObj.address = this.formValue.value.address;
    this.participantModelObj.mobile = this.formValue.value.mobile;
    this.participantModelObj.email = this.formValue.value.email;
    this.participantModelObj.emergencyName = this.formValue.value.emergencyName;
    this.participantModelObj.emergencyContact = this.formValue.value.emergencyContact;
    this.participantModelObj.referredBy = this.formValue.value.referredBy;
    this.participantModelObj.gender = this.formValue.value.gender;
    this.participantModelObj.dateOfBirth = this.formValue.value.dateOfBirth;
    this.participantModelObj.occupation = this.formValue.value.occupation;
    this.participantModelObj.education = this.formValue.value.education;
    this.participantModelObj.relationship = this.formValue.value.relationship;
    this.participantModelObj.dateOfJoining = this.formValue.value.dateOfJoining;
    this.participantModelObj.height = this.formValue.value.height;
    this.participantModelObj.weight = this.formValue.value.weight;
    this.participantModelObj.currentLevelOfActivity = this.formValue.value.currentLevelOfActivity;
    this.participantModelObj.currentRoutine = this.formValue.value.currentRoutine;
    this.participantModelObj.mostStress = this.formValue.value.mostStress;
    this.participantModelObj.pranayamaPractice = this.formValue.value.pranayamaPractice;
    this.participantModelObj.anySurgery = this.formValue.value.anySurgery;
    this.participantModelObj.anyMedical = this.formValue.value.anyMedical;
    this.participantModelObj.sleepQuality = this.formValue.value.sleepQuality;
    this.participantModelObj.smoked = this.formValue.value.smoked;
   
    this.participantModelObj.reasonForYoga = this.formValue.value.reasonForYoga;
    this.participantModelObj.musculoskeletal = this.formValue.value.musculoskeletal;
    this.participantModelObj.respiratory = this.formValue.value.respiratory;
    this.participantModelObj.cardiovascular = this.formValue.value.cardiovascular;
    this.participantModelObj.circulatory = this.formValue.value.circulatory;
    this.participantModelObj.neurological = this.formValue.value.neurological;
    this.participantModelObj.gastrointestinal = this.formValue.value.gastrointestinal;
    this.participantModelObj.endocrinological = this.formValue.value.endocrinological;
    this.participantModelObj.gynecologicalOrUrological = this.formValue.value.gynecologicalOrUrological;
    this.participantModelObj.pregnancy = this.formValue.value.pregnancy;
    this.participantModelObj.otherMedicalConditions = this.formValue.value.otherMedicalConditions;
    this.participantModelObj.otherInformations = this.formValue.value.otherInformations;
    this.participantModelObj.feedBack = '';
    this.participantModelObj.createdBy = localStorage.getItem('userId')?.toString();
   


    this.api.postStudent(this.participantModelObj)
      .subscribe({
        next: (res) => {
          console.log(res)

          this.folderId = res;

          
            this.uploader();

            
        
          this.router.navigate(['/participentDashBoard'])
          const msg = "New student is inserted"
          this.partiSerive.communicateParticipant(msg)
          this.formValue.reset();

        },
        error: () => {
          alert("Error while adding the Student")
        }
      })




  }


   uploader = async() => {
    


    
    for (let i = 0; i < this.fileSelected.length; i++) {

      

      this.FileModelObj.fileId = 0;
      this.FileModelObj.studentId = String(this.folderId);
      this.FileModelObj.fileName = this.fileSelected[i].name;
      this.FileModelObj.fileSize = String(this.fileSelected[i].size);

      var currentTime = new Date()

      // returns the month (from 0 to 11)
      var month = currentTime.getMonth() + 1

      // returns the day of the month (from 1 to 31)
      var day = currentTime.getDate()

      // returns the year (four digits)
      var year = currentTime.getFullYear()

      // write output MM/dd/yyyy
      var date = year + "-" + month + "-" + day

      this.FileModelObj.createDate = date

      
 
  this.http.put<any>(environment.fileApi + this.folderId + "/" + this.fileSelected[i].name, this.fileSelected[i])
  .subscribe({
    next: (res:any) => {
     

     
     
    },
    error: () => {
     
      alert("File upload failed")

    }
  })

    await this.fileMetaData(this.fileSelected[i].name,this.FileModelObj)

    }
  
   
    
  }



   fileMetaData = async(fileSelected:any, MetaData:any) =>{
    return new Promise((resolve, reject)=>{
      setTimeout(()=>{
       resolve(

        this.api.fileMetaData(MetaData).subscribe({
          next: () => {
           
           
          
          this.fileUploadCount++
         
          if(this.fileUploadCount == this.fileSelected.length) {
          
            
            this.router.navigate(['/participentDashBoard'])
          }

       

          },
          error: () => {
           
            alert("Failed to insert meta data of  " + fileSelected + "into DataBase");
          }
        })
       
       )
      },5000)
    })
    
  }


}
function ngOnInit() {
  throw new Error('Function not implemented.');
}

